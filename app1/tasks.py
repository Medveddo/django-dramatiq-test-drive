import time

import dramatiq
import requests


@dramatiq.actor
def count_words(url):
    print(f"Start counting words for {url}")
    time.sleep(5)
    response = requests.get(url)
    count = len(response.text.split(" "))
    print(f"There are {count} words at {url!r}.")
